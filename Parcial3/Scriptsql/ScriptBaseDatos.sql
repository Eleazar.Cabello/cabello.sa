create database WEB
use WEB;

create table Curp
(id smallint not null auto_increment,
    nombre varchar(30),
    ap_paterno varchar(30),
    ap_materno varchar(30),
    sexo varchar(5),
	fecha date,
    calle_numero varchar(50),
    colonia varchar(30),
    codigo_postal int,
    ciudad varchar(30),
    estado varchar(30),
    
    PRIMARY KEY(id)
);


INSERT into Curp (nombre,ap_paterno,ap_materno,sexo,fecha,calle_numero,colonia,codigo_postal,ciudad,estado)
VALUES ('Eleazar','Cabello','Sanchez','H','2000-03-02','cerro de la bufa #646','Colinas del sur','88021','Nuevo Laredo','Tamaulipas');

INSERT into Curp (nombre,ap_paterno,ap_materno,sexo,fecha,calle_numero,colonia,codigo_postal,ciudad,estado)
VALUES ('Juan','Perez','Renteria','H','1980-08-25','Canes #523','Fresnos','03981','Nuevo Leon','Monterrey');

INSERT into Curp (nombre,ap_paterno,ap_materno,sexo,fecha,calle_numero,colonia,codigo_postal,ciudad,estado)
VALUES ('Abigail','Trejo','Reyes','M','1999-09-15','Monterrey #839','Infonavit','95749','Nuevo Laredo','Tamaulipas');