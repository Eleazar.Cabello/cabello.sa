$(document).ready(function(){

    var parid;
    var bandera;
    //Desabilitar
    var botonE=document.getElementById('btnEliminar');
    botonE.disabled = true;
    var botonM=document.getElementById('btnModificar');
    botonM.disabled = true;
    var botonG=document.getElementById('btnGuardar');
    botonG.disabled = true;
    var INNombre=document.getElementById('Nombre');
    INNombre.disabled = true;
    var INSexo=document.getElementById('Sexo');
    INSexo.disabled = true;
    var INAPPaterno=document.getElementById('APPaterno');
    INAPPaterno.disabled = true;
    var INAPMaterno=document.getElementById('APMaterno');
    INAPMaterno.disabled = true;
    var INFecha=document.getElementById('Fecha');
    INFecha.disabled = true;
    var INCalleNumero=document.getElementById('CalleNumero');
    INCalleNumero.disabled = true;
    var INColonia=document.getElementById('Colonia');
    INColonia.disabled = true;
    var INPostal=document.getElementById('Postal');
    INPostal.disabled = true;
    var INCiudad=document.getElementById('Ciudad');
    INCiudad.disabled = true;
    var INEstado=document.getElementById('Estado');
    INEstado.disabled = true;

//CONSULTA------------------------------------
$('#btnConsulta').click(function() {
    $('#ModalConsulta').show();

    botonM.disabled = true;
    botonE.disabled = true;
    botonG.disabled = true;
    INNombre.disabled = true;
    INSexo.disabled = true;
    INAPPaterno.disabled = true;
    INAPMaterno.disabled = true;
    INFecha.disabled = true;
    INCalleNumero.disabled = true;
    INColonia.disabled = true;
    INPostal.disabled = true;
    INCiudad.disabled = true;
    INEstado.disabled = true;
    
    $('#btnCancelar').click(function() {
        $('#ModalConsulta').hide();
    })

    $('#btnAceptar').click(function() {
        parid =  document.getElementById("INConsulta").value;

        $.post('Consulta.PHP',{par1:parid},function(data){

            if(data.id == parid){
                refrescar(data);
                $('#ModalConsulta').hide();
                botonM.disabled = false;
                botonE.disabled = false;
            }else{
                $('#ModalConsulta').hide();
                $('#Mensaje').text("No existe ningun registro con ese ID.");
                $('#ModalMensaje').show();
                $('#btnAceptar1').click(function(){
                    $('#ModalMensaje').hide();
                })
                refrescar(data);
                botonM.disabled = true;
                botonE.disabled = true;
            }
        },'json');
    });
})

function refrescar(objeto) {
    console.log(objeto);
    $('#Nombre').val(objeto.nombre);
    $('#APPaterno').val(objeto.ap_paterno);
    $('#APMaterno').val(objeto.ap_materno);
    $('#Sexo').val(objeto.sexo);
    $('#Fecha').val(objeto.fecha);
    $('#CalleNumero').val(objeto.calle_numero);
    $('#Colonia').val(objeto.colonia);
    $('#Postal').val(objeto.codigo_postal);
    $('#Ciudad').val(objeto.ciudad);
    $('#Estado').val(objeto.estado);
}
    
//AGREGAR--------------------------------------------
$('#btnAgregar').click(function() {
    bandera =1;

    refrescar("undefined");
    botonM.disabled = true;
    botonE.disabled = true;
    botonG.disabled = false;
    INNombre.disabled = false;
    INSexo.disabled = false;
    INAPPaterno.disabled = false;
    INAPMaterno.disabled = false;
    INFecha.disabled = false;
    INCalleNumero.disabled = false;
    INColonia.disabled = false;
    INPostal.disabled = false;
    INCiudad.disabled = false;
    INEstado.disabled = false;
});

//GUARDAR--------------------------------------------------------------------
$('#btnGuardar').click(function() {

    console.log(bandera);
    if(bandera == 1){
        let objeto = {
            nombre: document.getElementById("Nombre").value,
            ap_paterno: document.getElementById("APPaterno").value,
            ap_materno: document.getElementById("APMaterno").value,
            sexo: document.getElementById("Sexo").value,
            fecha: document.getElementById("Fecha").value,
            calle_numero: document.getElementById("CalleNumero").value,
            colonia: document.getElementById("Colonia").value,
            codigo_postal: document.getElementById("Postal").value,
            ciudad: document.getElementById("Ciudad").value,
            estado: document.getElementById("Estado").value
        }
        console.log(objeto);
    
        $.post('Agregar.PHP',{
            nombre: objeto.nombre,
                ap_paterno: objeto.ap_paterno,
                ap_materno: objeto.ap_materno,
                sexo: objeto.sexo,
                fecha: objeto.fecha,
                calle_numero: objeto.calle_numero,
                colonia: objeto.colonia,
                codigo_postal: objeto.codigo_postal,
                ciudad: objeto.ciudad,
                estado: objeto.estado
            })
    
            $('#Mensaje').text("Se agrego correctamente el registro.");
            $('#ModalMensaje').show();
            $('#btnAceptar1').click(function(){
                $('#ModalMensaje').hide();
            })
    
        botonG.disabled = true;
        INNombre.disabled = true;
        INSexo.disabled = true;
        INAPPaterno.disabled = true;
        INAPMaterno.disabled = true;
        INFecha.disabled = true;
        INCalleNumero.disabled = true;
        INColonia.disabled = true;
        INPostal.disabled = true;
        INCiudad.disabled = true;
        INEstado.disabled = true;
    }

    if(bandera == 0){
        let objeto = {
            nombre: document.getElementById("Nombre").value,
            ap_paterno: document.getElementById("APPaterno").value,
            ap_materno: document.getElementById("APMaterno").value,
            sexo: document.getElementById("Sexo").value,
            fecha: document.getElementById("Fecha").value,
            calle_numero: document.getElementById("CalleNumero").value,
            colonia: document.getElementById("Colonia").value,
            codigo_postal: document.getElementById("Postal").value,
            ciudad: document.getElementById("Ciudad").value,
            estado: document.getElementById("Estado").value
        }
        console.log(parid);

        $.post('Modificar.PHP',{par1:parid,
            nombre: objeto.nombre,
            ap_paterno: objeto.ap_paterno,
            ap_materno: objeto.ap_materno,
            sexo: objeto.sexo,
            fecha: objeto.fecha,
            calle_numero: objeto.calle_numero,
            colonia: objeto.colonia,
            codigo_postal: objeto.codigo_postal,
            ciudad: objeto.ciudad,
            estado: objeto.estado})

        $('#Mensaje').text("Se edito correctamente.");
        $('#ModalMensaje').show();
        $('#btnAceptar1').click(function(){
            $('#ModalMensaje').hide();
        })
        refrescar("undefined");
        botonM.disabled = true;
        botonE.disabled = true;
        botonG.disabled = true;
        INNombre.disabled = true;
        INSexo.disabled = true;
        INAPPaterno.disabled = true;
        INAPMaterno.disabled = true;
        INFecha.disabled = true;
        INCalleNumero.disabled = true;
        INColonia.disabled = true;
        INPostal.disabled = true;
        INCiudad.disabled = true;
        INEstado.disabled = true;
    }
   
});

//ELIMINAR--------------------------------------------------------------------------
$('#btnEliminar').click(function() {

    console.log(parid);
    $.post('Consulta.PHP',{par1:parid},function(data){
        console.log(data);
        $('#Mensaje2').text("¿Desea eliminar el siguiente registro?:     "+data.nombre+", "+
        data.ap_paterno+", "+data.ap_materno+", "+data.sexo+
        ", "+data.fecha+", "+data.calle_numero+", "+data.colonia+
        ", "+data.codigo_postal+", "+data.ciudad+", "+data.estado);

        $('#ModalConfirmar').show();

    },'json');
   
    $('#btnAceptar2').click(function() {
        $('#ModalConfirmar').hide();
        $.post('Eliminar.PHP',{par1:parid});
        $('#Mensaje').text("Se elimino correctamente.")
        $('#ModalMensaje').show();

        $('#btnAceptar1').click(function(){
            $('#ModalMensaje').hide();
        })

        refrescar("undefined");
        botonM.disabled = true;
        botonE.disabled = true;
    });

    $('#btnCancelar2').click(function() {
        $('#ModalConfirmar').hide();
    });
 });



//MODIFICAR--------------------------------------------------------------------
    $('#btnModificar').click(function() {
        bandera = 0;

        botonG.disabled = false;
        botonE.disabled = true;

        INNombre.disabled = false;
        INSexo.disabled = false;
        INAPPaterno.disabled = false;
        INAPMaterno.disabled = false;
        INFecha.disabled = false;
        INCalleNumero.disabled = false;
        INColonia.disabled = false;
        INPostal.disabled = false;
        INCiudad.disabled = false;
        INEstado.disabled = false;

    });
});

